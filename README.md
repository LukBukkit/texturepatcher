# README #



### Found Bugs? ###
1. Look at the wiki!
2. If you didn't find an answer, use the [Issue Tracker](https://bitbucket.org/LukBukkit/texturepatcher/issues/new) and I try to fix this bug!

### Libraries ###
* zip4j_1.3.2.jar
* AbsoluteLayout.jar (NetBeans)

### BuildScript ###
Using Ant!